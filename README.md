# Magang SI 2023

**Design Aplikasi Digital Library**

# Introduction
Nama    : Zaydan Aymar Luthfy 

NIM     : 2000016020

Posisi  : Designer Divisi Creative

Lokasi  : PT. Woolu Aksara Maya

# RoadMap
**Main Tasks**
- [iPustaka Kerinci](https://gitlab.com/Zaydanaymar/magang-si-2023/-/milestones/3#tab-issues)
-  [iSukamara](https://gitlab.com/Zaydanaymar/magang-si-2023/-/milestones/4#tab-issues)
-  [Tapin Cerdas](https://gitlab.com/Zaydanaymar/magang-si-2023/-/milestones/7#tab-issues)

**Additional Tasks**
-  [Map Infografis](https://gitlab.com/Zaydanaymar/magang-si-2023/-/milestones/5#tab-issues)
-  [Edoo Merchandise](https://gitlab.com/Zaydanaymar/magang-si-2023/-/milestones/6#tab-issues)
